﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motion : MonoBehaviour {
    Rigidbody rb;
    Vector3 force;
    private float speed = 2;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();

    }
    void FixedUpdate()
    {
        float movehor = Input.GetAxis("Horizontal");
        float movever = Input.GetAxis("Vertical");
        Vector3 move = new Vector3(movehor, 0.1f, movever);
        rb.AddForce(move * speed);
    }
    // Update is called once per frame
    void Update () {
       // rb.AddForce(force) ;
	}
}
