﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballrotate : MonoBehaviour {
    private Vector3 rot;
    private float val;
    private Quaternion Rotate;
	// Use this for initialization
	void Start () {
        rot.x = Random.Range(-30,30);
        rot.y = Random.Range(-90,90);
        rot.z = 0;
	}
	
	// Update is called once per frame
	void Update () {
        val = Random.RandomRange(1, 25);
        Rotate = (Random.rotation);
        transform.Rotate(rot* Time.deltaTime * val, Space.World);
        // transform.Rotate(rot * Time.deltaTime*3, Space.World);
    }
}
